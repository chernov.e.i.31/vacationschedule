﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using VacationSchedule.NotificationService.Model;

namespace VacationSchedule.NotificationService.Services
{
    public class EmailSendingService : IEmailSendingService
    {
        private readonly SmtpServerSettings _smtpServerSettings;
        private Serilog.ILogger _logger;
        public EmailSendingService(SmtpServerSettings smtpServerSettings, Serilog.ILogger logger)
        {
            _smtpServerSettings = smtpServerSettings;
            _logger = logger;
        }
        public async Task SendEmailAsync(SendEmailRequest emailRequest)
        {
            try
            {
                var email = new MimeMessage();
                email.Sender = MailboxAddress.Parse(_smtpServerSettings.From);
                email.To.Add(MailboxAddress.Parse(emailRequest.MailTo));
                email.Subject = emailRequest.Subject;
                var builder = new BodyBuilder();

                builder.HtmlBody = emailRequest.Message;
                email.Body = builder.ToMessageBody();
                using (var smtp = new SmtpClient())
                {
                    smtp.Connect(_smtpServerSettings.SmtpServer, _smtpServerSettings.Port, SecureSocketOptions.StartTls);
                    smtp.Authenticate(_smtpServerSettings.From, _smtpServerSettings.Password);
                    await smtp.SendAsync(email);
                    smtp.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
            }
        }
    }
}
