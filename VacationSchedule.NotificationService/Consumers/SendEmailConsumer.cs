﻿using MassTransit;
using VacationSchedule.Core.Contracts;
using VacationSchedule.NotificationService.Mappers;
using VacationSchedule.NotificationService.Services;

namespace VacationSchedule.NotificationService.Consumers
{
    public class SendEmailConsumer : IConsumer<EmailContract>
    {
        private readonly IEmailSendingService _emailSendingService;
        Serilog.ILogger _logger;

        public SendEmailConsumer(IEmailSendingService emailSendingService, Serilog.ILogger logger)
        {
            _emailSendingService = emailSendingService;
            _logger = logger;
        }

        public async Task Consume(ConsumeContext<EmailContract> context)
        {
            try
            {
                await _emailSendingService.SendEmailAsync(EmailContractToSendEmailRequestMapper.Map(context.Message));
            }
            catch (Exception ex)
            {
                _logger.Error(ex, $"An error occurred.");
            }
        }
    }
}
