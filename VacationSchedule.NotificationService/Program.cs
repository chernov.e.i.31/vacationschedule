using MassTransit;
using Serilog;
using System.Security.Authentication;
using VacationSchedule.NotificationService.Consumers;
using VacationSchedule.NotificationService.Model;
using VacationSchedule.NotificationService.Services;

namespace VacationSchedule.NotificationService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            
            builder.Host.ConfigureServices((hostContext, services) =>
            {
                IConfiguration configuration = hostContext.Configuration;

                //������
                Log.Logger = new LoggerConfiguration()
                        .WriteTo.Console()
                        .CreateLogger();

                services.AddSingleton(Log.Logger);

                //��������� smtp
                var smtpServerSettings = configuration.GetSection("SmtpServerSettings").Get<SmtpServerSettings>();
                services.AddSingleton(smtpServerSettings);

                //������ �������� �����
                services.AddScoped<IEmailSendingService, EmailSendingService>();

                //-- �������� ��� �������
                var rabbitMqProtocol = configuration["RabbitMQ:RabbitMQ_Protocol"] ?? string.Empty;
                var rabbitMqHost = configuration["RabbitMQ:RabbitMQ_Host"] ?? string.Empty;
                var rabbitMqPort = configuration["RabbitMQ:RabbitMQ_Port"] ?? string.Empty;
                var rabbitMqUser = configuration["RabbitMQ:RabbitMQ_User"] ?? string.Empty;
                var rabbitMqPass = configuration["RabbitMQ:RabbitMQ_Pass"] ?? string.Empty;

                services.AddMassTransit(configurator =>
                {
                    configurator.AddConsumer<SendEmailConsumer>();
                    configurator.UsingRabbitMq((context, rabbitConfigurator) =>
                    {
                        rabbitConfigurator.Host(new Uri($"{rabbitMqProtocol}://{rabbitMqUser}:{rabbitMqPass}@{rabbitMqHost}/{rabbitMqUser}"));

                        rabbitConfigurator.ConfigureEndpoints(context);
                    });
                });

                services.AddAuthorization();
                services.AddControllers();
            }
            );

            // Add services to the container.
            
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen();

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}