﻿using MessageService.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MessageService.Classes
{
    // Базовый класс:
    public class Message : IMessage
    {
        public async Task Send() { }
    }
}
