﻿using System.Net;
using System.Net.Mail;

namespace MessageService.Classes
{
    public class EmailMessage : Message
    {
        // Задаем точку подключения для нашей почты: 
        SmtpClient client = new("smtp.gmail.com");
        // Используется для передачи адресов from / to:
        MailMessage message = new();
        // Добавление from / to адресов для отправки почтовых уведомлений:
        MailAddress from = new MailAddress("vacationschedule.message@gmail.com", "Сервис отправки уведомлений");    // Адрес откуда отправляем
        MailAddress to = new MailAddress("vacationschedule.message@gmail.com", "Сервис отправки уведомлений");      // Адрес куда отправляем
        DateTime _dateTimeNow = DateTime.Now;                                                                       // Текущая дата

        public async Task Send(string inputName, string inputPatronymic, string inputEmail, DateTime inputDate)
        {
            try
            {
                // Задаем настройки для smtp-clienta с SSL авторизацией:
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("vacationschedule.message@gmail.com", "yvszeqryzuzpbxju"); // Для входа в почту используйте пароль: a11111111!
                client.Host = "smtp.gmail.com";
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                // Добавляем возможность ответа на письмо пользователем:
                MailAddress replyTo = new MailAddress("vacationschedule.message@gmail.com");
                message.ReplyToList.Add(replyTo);
                // Добавляем к текущей дате 14 дней:
                _dateTimeNow = _dateTimeNow.AddDays(14);                         // К DateTime.Now добавляем 14 дней.                
                if (_dateTimeNow.Date == inputDate.Date)
                {
                    // Передаем почту:
                    to = new(inputEmail);
                    message = new(from, to);
                    // Задаем тему письма и кодировку:
                    message.Subject = "Уведомление о начале опуска";
                    message.SubjectEncoding = System.Text.Encoding.UTF8;
                    // Задаем текст сообщения, кодировку и формат отправки (text или HTML):
                    message.IsBodyHtml = true;
                    message.Body = $"<b>Уважаемый {inputName} {inputPatronymic}.</b><br>Напоминаем вам, что дата начала вашего отпуска назначена на {inputDate.ToShortDateString()}.<br>";
                    message.BodyEncoding = System.Text.Encoding.UTF8;
                    // Отправляем письмо:
                    client.Send(message);
                    Console.WriteLine($"Письмо отправлено на почту {inputEmail}.");
                    _dateTimeNow = DateTime.Now;                                // Сбрасываем на текущую дату.
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
