﻿using VacationSchedule.Core.Contracts;
using VacationSchedule.NotificationService.Model;

namespace VacationSchedule.NotificationService.Mappers
{
    public static class EmailContractToSendEmailRequestMapper
    {
        public static SendEmailRequest Map(EmailContract source)
        {
            return new SendEmailRequest
            {
                MailTo = source.MailTo,
                Subject = source.Subject,
                Message = source.Message,
            };
        }
    }
}
