﻿namespace VacationSchedule.Core.Contracts
{
    public class EmailContract
    {
        public string MailTo { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
