﻿namespace VacationSchedule.Core.Models.DB
{
    public class Log
    {
        public int Id { get; set; }
        public DateTime LogDateTime { get; set; }
        public string? LogMessage { get; set; }
    }
}
