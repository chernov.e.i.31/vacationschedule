﻿namespace VacationSchedule.Core.Models.DB
{
    public class User
    {
        public int Id { get; set; }
        public string? Login { get; set; }
        public string? Password { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? PatronymicName { get; set; }
        public string? Position { get; set; }


        public int? RoleId { get; set; }                     // внешний ключ
        public Role? Role { get; set; }                     // навигационное свойство на таблицу Role для EFCore
        public int? DepartmentId { get; set; }               // внешний ключ
        public Department? Department { get; set; }         // навигационное свойство на таблицу Department для EFCore
        public int? HeadDepartmentId { get; set; }
        public Department? HeadDepartment { get; set; }         // навигационное свойство на таблицу Department для EFCore


        public List<UserContact>? UserContacts { get; set; }
        public List<Vacation>? Vacations { get; set; }

        public User()
        {
            UserContacts = new List<UserContact>();
            Vacations = new List<Vacation>();
        }
    }
}
