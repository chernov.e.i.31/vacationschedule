﻿namespace VacationSchedule.Core.Models.DB
{
    public class VacationType
    {
        public int Id { get; set; }
        public string? Name { get; set; }
    }
}
