﻿namespace VacationSchedule.Core.Models.DB
{
    public class Role
    {
        public int Id { get; set; }
        public string? RoleName { get; set; }
        public bool IsAdminRights { get; set; }
        public bool IsHeadOfDepartmentRights { get; set; }
    }
}
