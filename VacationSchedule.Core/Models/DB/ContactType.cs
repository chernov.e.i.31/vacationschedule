﻿namespace VacationSchedule.Core.Models.DB
{
    public class ContactType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserContact> Contacts { get; set; }

        public ContactType()
        {
            Contacts = new List<UserContact>();
        }
    }
}
