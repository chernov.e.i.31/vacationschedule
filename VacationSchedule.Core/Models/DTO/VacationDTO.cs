﻿namespace VacationSchedule.Core.Models.DTO
{
    public class VacationDTO
    {
        public int Id { get; set; }
        public DateOnly VacationStartDate { get; set; }
        public DateOnly VacationEndDate { get; set; }
        public string VacationType { get; set; }
        public bool IsAccepted { get; set; }
        public string? Comment { get; set; }
    }
}
