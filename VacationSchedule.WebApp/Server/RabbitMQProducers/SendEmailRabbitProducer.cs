﻿using MassTransit;
using VacationSchedule.Core.Contracts;

namespace VacationSchedule.WebApp.Server.RabbitMQProducers
{
    public class SendEmailRabbitProducer: ISendEmailRabbitProducer
    {

        private readonly IBus _bus;
        private readonly Serilog.ILogger _logger;

        public SendEmailRabbitProducer(IBus bus, Serilog.ILogger logger)
        {
             _bus = bus;
            _logger= logger;
        }

        public async Task Send<IEmailContract>(IEmailContract message)
        {
            try
            {
                await _bus.Publish<EmailContract>(message);
                _logger.Information($"{DateTime.Now}: SendEmailRabbitProducer: Message is send to queue");
            }
            catch(Exception ex)
            {
                _logger.Error(ex.ToString());
            }
        }
    }
}
