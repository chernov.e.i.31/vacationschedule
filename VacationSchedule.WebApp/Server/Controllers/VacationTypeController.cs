﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VacationSchedule.Core.Models.DB;
using VacationSchedule.DBEntity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VacationSchedule.WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VacationTypeController : ControllerBase
    {
        private readonly PostgreBdContext _db;
        public VacationTypeController(PostgreBdContext db)
        {
            this._db = db;
        }
        // GET: api/<ValuesController>
        [HttpGet]
        public IEnumerable<VacationType> Get()
        {
            return _db.VacationTypes;
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public VacationType? Get(int id)
        {
            return _db.VacationTypes.Where(x => x.Id == id).FirstOrDefault();
        }

        // POST api/<ValuesController>
        [HttpPost]
        public void Post([FromBody] VacationType value)
        {
            if (value.Id == 0)
            {
                _db.VacationTypes.Add(value);
                _db.SaveChanges();
            }
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] VacationType value)
        {
            if (_db.VacationTypes.Where(x => x.Id == id).AsNoTracking().FirstOrDefault() is not null)
            {
                _db.VacationTypes.Update(value);
                _db.SaveChanges();
            }
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
