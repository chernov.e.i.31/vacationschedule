﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using VacationSchedule.Core.Models.DB;
using VacationSchedule.DBEntity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VacationSchedule.WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VacationController : ControllerBase
    {
        private readonly PostgreBdContext _db;
        public VacationController(PostgreBdContext db)
        {
            this._db = db;
        }
        // GET: api/<ValuesController>
        [HttpGet]
        public ActionResult<IEnumerable<Vacation>> Get()
        {
            return Ok(_db.Vacations);
        }

        // GET api/<ValuesController>/5
        [HttpGet("{id}")]
        public Vacation? Get(int id)
        {
            return _db.Vacations.Where(x => x.Id == id).FirstOrDefault();
        }

        [HttpGet("GetByUserId")]
        public IEnumerable<Vacation?> GetByUserId(int id)
        {
            return _db.Vacations
                .Where(x => x.UserId == id)
                .Include(x => x.VacationType);
        }

        [HttpGet("GetVacationInDepByUserId")]
        public IEnumerable<User?> GetVacationInDepByUserId(int id)
        {
            var vac = new List<User>();

            var user = _db.Users.Where(x => x.Id == id).FirstOrDefault();
            if (user is User)
            {
                if (user.DepartmentId is not null)
                {
                    return GetVacationsByDep(user.DepartmentId, (user.HeadDepartmentId is not null ? true : false));
                }
            }
            return vac;
        }

        [HttpPut("ConfirmVacation")]
        public ActionResult ConfirmVacation([FromBody] Vacation value)
        {
            var vacation = _db.Vacations.Where(x => x.Id == value.Id).FirstOrDefault();
            if (vacation is Vacation)
            {
                vacation.Comment = value.Comment;
                if (value.IsAccepted)
                {
                    if (CheckVacationInsert(vacation, true))
                    {
                        vacation.IsAccepted = true;
                        _db.Update(vacation);
                        _db.SaveChanges();
                        return Ok();
                    }
                    else
                    {
                        return BadRequest("Имеется пересечение в периодах");
                    }
                }
                else
                {
                    _db.Update(vacation);
                    _db.SaveChanges();
                    return Ok();
                }
            }
            return BadRequest("Период не найден");
        }

        private IEnumerable<User> GetVacationsByDep(int? depId, bool recursive = false)
        {
            var vac = _db.Departments.Where(x => x.Id == depId)
                .Include(x => x.SubDepartments)
                .Include(x => x.Users)
                .ThenInclude(x => x.Vacations)
                .ThenInclude(x => x.VacationType)
                .FirstOrDefault();
            var users = vac.Users;
            if (recursive)
            {
                foreach (var subD in vac.SubDepartments)
                {
                    users.AddRange(GetVacationsByDep(subD.Id, recursive));
                }
            }
            return users;
        }

        // POST api/<ValuesController>
        [HttpPost]
        public ActionResult Post([FromBody] Vacation value)
        {
            if (value.Id == 0)
            {
                if (value.VacationType.Id == -1)
                {
                    return BadRequest("Не выбран вид отдыха");
                }
                if (value.VacationStartDate > value.VacationEndDate)
                {
                    return BadRequest("Не верно введен период");
                }
                if (CheckVacationInsert(value))
                {
                    value.VacationTypeId = value.VacationType.Id;
                    value.VacationType = null;
                    _db.Vacations.Add(value);
                    _db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest("Имеется пересечение в периодах");
                }
            }
            return BadRequest();
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Vacation value)
        {
            if (_db.Vacations.Where(x => x.Id == id).AsNoTracking().FirstOrDefault() is Vacation)
            {
                _db.Vacations.Update(value);
                _db.SaveChanges();
            }
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var vac = _db.Vacations.Where(x => x.Id == id).FirstOrDefault();
            if (vac is Vacation)
            {
                if (!vac.IsAccepted)
                {
                    _db.Vacations.Remove(vac);
                    _db.SaveChanges();
                    return Ok();
                }
                else
                {
                    return BadRequest("Запись утверждена");
                }
            }
            return BadRequest("Запись не найдена");
        }

        private bool CheckVacationInsert(Vacation vacation, bool vacType = false)
        {
            var depId = _db.Users.Where(x => x.Id == vacation.UserId).Select(x => x.DepartmentId).FirstOrDefault();
            if (depId is not null)
            {
                var users = _db.Users.Include(x => x.Vacations).Where(x => x.DepartmentId == depId);
                List<Vacation> vacList = new();
                foreach (var u in users)
                {
                    vacList.AddRange(u.Vacations);
                }
                vacList.Remove(vacation);
                var unionVac = vacList
                    .Where(x =>
                        (vacation.VacationStartDate >= x.VacationStartDate && vacation.VacationStartDate <= x.VacationEndDate)
                        || (vacation.VacationEndDate >= x.VacationStartDate && vacation.VacationEndDate <= x.VacationEndDate));
                if (vacType == true)
                {
                    unionVac.Where(x => x.IsAccepted == true);
                }
                return unionVac.FirstOrDefault() is Vacation ? false : true;
            }
            return false;
        }
    }
}
