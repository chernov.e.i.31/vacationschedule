﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VacationSchedule.Core.Models.DB;
using VacationSchedule.DBEntity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace VacationSchedule.WebApp.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        private readonly PostgreBdContext _db;
        public DepartmentController(PostgreBdContext db)
        {
            this._db = db;
        }
        // GET: api/<DepartmentController>
        [HttpGet]
        public IEnumerable<Department> Get()
        {
            return _db.Departments
                .Include(x => x.HeadOfDepartmentUser)
                .Include(x => x.Parent);
        }

        // GET api/<DepartmentController>/5
        [HttpGet("{id}")]
        public Department? Get(int id)
        {
            return _db.Departments.Where(x => x.Id == id).FirstOrDefault();
        }

        // POST api/<DepartmentController>
        [HttpPost]
        public void Post([FromBody] Department value)
        {
            if (value.Id == 0)
            {
                value.ParentId = value.Parent?.Id;
                value.Parent = null;
                _db.Departments.Add(value);
                _db.SaveChanges();
            }
        }

        // PUT api/<ValuesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] Department value)
        {
            var dep = _db.Departments.Where(x => x.Id == id).Include(x => x.HeadOfDepartmentUser).AsNoTracking().FirstOrDefault();
            if (dep is not null)
            {
                if (dep.HeadOfDepartmentUser?.Id != value.HeadOfDepartmentUser.Id)
                {
                    if (dep.HeadOfDepartmentUser is not null)
                    {
                        // обнуляем текущего главу департамента
                        var usr = dep.HeadOfDepartmentUser;
                        usr.HeadDepartmentId = null;
                        usr.HeadDepartment = null;
                        _db.Users.Update(usr);
                        _db.SaveChanges();
                    }
                    var currenHead = _db.Users.Where(x => x.Id == value.HeadOfDepartmentUser.Id).FirstOrDefault();
                    if (currenHead is not null)
                    {
                        currenHead.HeadDepartmentId = value.Id;
                        _db.Users.Update(currenHead);
                        _db.SaveChanges();
                        value.HeadOfDepartmentUser = null;
                    }
                }
                value.HeadOfDepartmentUser = null;
                _db.Departments.Update(value);
                _db.SaveChanges();
            }
        }

        // DELETE api/<ValuesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
