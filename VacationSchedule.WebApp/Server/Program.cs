using Hangfire;
using Hangfire.PostgreSql;
using MassTransit;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;
using Serilog;
using VacationSchedule.DBEntity;
using VacationSchedule.DBIdentity;
using VacationSchedule.DBIdentity.Model;
using VacationSchedule.WebApp.Server.Notification;
using VacationSchedule.WebApp.Server.RabbitMQProducers;

var builder = WebApplication.CreateBuilder(args);

IConfiguration configuration = builder.Configuration;

// TODO ������� ��
// Update-Database -Context ApplicationDBContext
// Update-Database -Context PostgreBdContext
builder.Services.AddDbContext<PostgreBdContext>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection")));
builder.Services.AddDbContext<ApplicationDBContext>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("IdentityConnection")));
builder.Services.AddIdentity<ApplicationUser, IdentityRole>().AddEntityFrameworkStores<ApplicationDBContext>();
builder.Services.AddHangfire(x => x.UsePostgreSqlStorage(builder.Configuration.GetConnectionString("HangfireConnection")));
builder.Services.ConfigureApplicationCookie(options =>
{
    options.Cookie.HttpOnly = false;
    options.Events.OnRedirectToLogin = context =>
    {
        context.Response.StatusCode = 401;
        return Task.CompletedTask;
    };
});
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo { Title = "VacationSchedule", Version = "v1" });
});
builder.Services.AddControllers()
    .AddNewtonsoftJson(options =>
    {
        options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
    });
builder.Services.AddControllersWithViews();
builder.Services.AddRazorPages();

//������
Log.Logger = new LoggerConfiguration()
    .WriteTo.Console()
    .CreateLogger();

builder.Services.AddSingleton(Log.Logger);

//-- ����������� ��� �������
var rabbitMqProtocol = configuration["RabbitMQ:RabbitMQ_Protocol"] ?? string.Empty;
var rabbitMqHost = configuration["RabbitMQ:RabbitMQ_Host"] ?? string.Empty;
var rabbitMqPort = configuration["RabbitMQ:RabbitMQ_Port"] ?? string.Empty;
var rabbitMqUser = configuration["RabbitMQ:RabbitMQ_User"] ?? string.Empty;
var rabbitMqPass = configuration["RabbitMQ:RabbitMQ_Pass"] ?? string.Empty;

builder.Services.AddMassTransit(configurator =>
{
    configurator.UsingRabbitMq((context, rabbitConfigurator) =>
    {
        rabbitConfigurator.Host(new Uri($"{rabbitMqProtocol}://{rabbitMqUser}:{rabbitMqPass}@{rabbitMqHost}/{rabbitMqUser}"));

        rabbitConfigurator.ConfigureEndpoints(context);
    });
});

builder.Services.AddScoped<ISendEmailRabbitProducer, SendEmailRabbitProducer>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseWebAssemblyDebugging();
    app.UseSwagger();
    app.UseSwaggerUI(c =>
        c.SwaggerEndpoint("/swagger/v1/swagger.json",
        "VacationSchedule v1"));
}
else
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();

app.UseBlazorFrameworkFiles();
app.UseStaticFiles();
app.UseHangfireDashboard();
app.UseHangfireServer();

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();

app.MapRazorPages();
app.MapControllers();
app.MapFallbackToFile("index.html");

//TODO ������� ������ ���� � ������ Hangfire �� ������� ����������� ������� �������� /hangfire
JobStorage.Current = new PostgreSqlStorage(builder.Configuration.GetConnectionString("HangfireConnection"));
using (var serviceScope = app.Services.CreateScope())
{
    var services = serviceScope.ServiceProvider;
    RecurringJob.AddOrUpdate<NotificationManager>(
        x => x.Send(),
        cronExpression: Cron.Daily,
        timeZone: TimeZoneInfo.FindSystemTimeZoneById("Russian Standard Time")
    );
}
app.Run();