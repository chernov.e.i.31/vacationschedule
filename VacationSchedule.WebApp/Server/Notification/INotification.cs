﻿namespace VacationSchedule.WebApp.Server.Notification
{
    public interface INotification
    {
        void Send(IEnumerable<string> address, string subject, string message);
    }
}
